The **encryptr** package

The package `encryptr` contains easy-to-use functions, which allow users to encrypt and decrypt strings. The two primary functions are `encrypt_string` and `decrypt_string`.

`encrypt_string` has two main arguments. The first one is `string =`, which receives the character string that the user wants to encrypt. The second main argument is `password = `, which exclusively receives integers. This argument is important because it encrypts the user's string in a unique way. That is to say that a string, say `"Hello"`, will be encrypted in different ways if a different password is used in the encryption process. The argument's default value is `123`.

```{r}
encrypt_string(string = "Hello", password = 123)
encrypt_string(string = "Hello", password = 125)
```

`decrypt_string` has two main arguments also. `encrypted =` only accepts objects of the class `encrypted`. For full decryption, it needs to be paired with the right password. `password =` receives the password used to create the `encrypted` object. The function will output an error if the wrong password is passed.
